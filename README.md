# Мониторринг настроен с помощью Prometheus Stack с использованием Helm Chart и уведомлением в Telegram. Логирование реализовано с помощью Grafana Loki.

## Установка kube-prometheus-stack в кластер
**Установка prometheus-stack была выполнена с помощью Helm**
-  kubectl create namespace monitoring - Создание namespace для prometheus-stack
-  helm install kube-prom-stack prometheus-community/kube-prometheus-stack -n monitoring -f prometheus-alert-values.yaml - Установка prometheus-stack с интеграцией настроек для alert-manager для оповещения в телеграм.
-  kubectl --namespace monitoring get pods -l "release=kube-prom-stack" - Проверка запущенных подов prometheus-stack
-  kubectl apply -f grafana-values.yaml -n monitoring - Установить сервис для grafana, чтобы подключиться к веб интерфейсу
-  Перейти по адресу http://130.193.40.112:30115 
-  Авторизоваться в веб-интерфейсе Grafana - username: admin, password: prom-operator. Следует изменить пароль после Авторизации.
